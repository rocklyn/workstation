#!/bin/bash
set -x

source /etc/environment.d/ddns

security() {
    apt-get install lynis
    cat <<'EOF' >${1}/etc/script/lynis.cron
#!/bin/bash
tmp=\$(mktemp)
ip=\$(curl -4s "https://ident.me")
lynis -c -Q >\${tmp}
mail -s "lynix ${MAIL_ADDRESS}" -a ${tmp}
rm -f \${tmp}

EOF
    chmod +x ${1}/etc/script/lynis.cron
    (( SKIP_PKGS_SECURITY )) || printf "@daily /etc/script/lynis.cron" >${1}/etc/cron.d/lynis

}

[[ -n ${DDNSURL} ]] && curl -k ${DDNSURL} &>/dev/null
