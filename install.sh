#!/bin/bash

DDNSURL=""

ddns() {
    cat <<EOF>/etc/systemd/system/ddns.service
[Unit]
Description=Update from repository
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=root
ExecStart=/usr/bin/curl -qk ${DDNSURL}

[Install]
WantedBy=multi-user.target

EOF

    systemctl daemon-reload
    systemctl enable ddns.service
    systemctl restart ddns.service
}

read -ep "DDNS: " DDNSURL
[[ -n ${DDNSURL} ]] && ddns
